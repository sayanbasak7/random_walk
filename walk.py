#!/usr/bin/env python
# coding: utf-8

# ## Section 2:
# ### Consider a grid in d-dimensional space. There are n grid lines in each dimension, spaced one unit apart. We will consider a walk of m steps from grid intersection to grid intersection. Each step will be a single unit movement in any one of the dimensions, such that it says on the grid. We wish to look at the number of possible paths from a particular starting location on this grid.
# 
# ### For example, consider the case where d=2 and n=3. We will label the grid intersections (x,y), where x,y E {0,1,2}. There will be six valid walks starting at (0,0) of length m=2:
# 
# * (0,0)->(0,1)->(0,0)
# * (0,0)->(0,1)->(0,2)
# * (0,0)->(0,1)->(1,1)
# * (0,0)->(1,0)->(0,0)
# * (0,0)->(1,0)->(2,0)
# * (0,0)->(1,0)->(1,1)
# 
# ### Note that the walks may double back upon themselves, and multiple walks may end at the same grid intersection. All of these walks are counted.


import pandas as pd
import numpy as np
import time
from os import path

#------------------- Some key points -------------------#

# These functions and algorithms are defined keeping in mind the symmetries associated with the given lattice.
# Configurationally the site index 'i' is equivalent to 'n-i', hence for even number of lattice sites for each degree of freedom, the relevant sites are reduced to (n/2)^d.
# Also, another reduction in the number of relevant sites is achieved by:
# The number of valid walks originating from (i1,i2,...,id) s.t. i1 <= i2 <= ... <= id, is same for all the permutations, hence their relative weights are the number of permutations of (i1,i2,...,id).
# For the case that n is odd, relevant sites are reduced by ((n+1)/2)^d, also for each site that contain i(s)=(n+1)/2 the relative weights goes down by a factor of 2. 

#-------------------------------------------------------#


#-- Functions --#

def position_indexing(pos, d, n):
    return np.sum(pos*(n**np.array(range(d),dtype=int)[::-1]), axis=-1)

def index_positioning(indx, d, n):
    return [int(indx/(n**(d-1-i)))%n for i in range(d)]

def perm(s):
    if len(s)==0:
        return 1
    return len(s)*perm(s[1:])/s.count(s[0])

def uniq_perm(x):
    s = ''.join([chr(xi+ord('A')) for xi in x])
    return int(perm(s))

def countNhalf(x,n):
    return np.sum([int(xi==int((n-1)/2)) for xi in x])

def next_locs(start, d, n):
    locs = []
    move = np.diag(np.ones(d,dtype=int))
    for i in range(d):
        if (start[i] > 0):
            locs.append(np.sort(start-move[i]))
        if (start[i] < int((n-1)/2)):
            locs.append(np.sort(start+move[i]))
        else:
            if (start[i] < n-1):
                x = start
                x[i] = n-1-x[i]-1
                locs.append(np.sort(x))
    return locs

p_dict = {}
i_dict = {}
mapSize = {}
next_moves = {}

def i_map_p(d, d_dep, n_half, n_dep, count, pos):
    if (d_dep):
        for i in range(n_dep,n_half):
            pos[d-d_dep:] = i#*np.ones(d_dep)

            count = i_map_p(d, d_dep-1, n_half, i, count, pos)
#             count += (d_dep==1)
            
    else:
#         print(pos,sep='',end=',',flush=True)
        p_dict[str([count,d,n_half])] = np.array(pos)
        i_dict[str([np.array(pos),n_half])] = count
        return count+1
    
    return count

def full_map(d,n):
    n_half = int((n+1)/2)
    
    map_size = 0
    try:
        map_size = mapSize[str([d,n_half])]
        
    except:
        print("Position <-> Index -",end='')
        t1 = time.time()
        map_size = i_map_p(d,d,n_half,0,0,np.zeros(d,dtype=int))
        mapSize[str([d,n_half])] = map_size
        print("- Mapped. t =",time.time()-t1)
        
    try:
        _ = next_moves[str([0,d,n])]
    except:
        print("Moves -",end='')
        t1 = time.time()
        for i in range(map_size):
            p = p_dict[str([i,d,n_half])]
            p_next = next_locs(p,d,n)
            i_next = [i_dict[str([np.array(p_),n_half])] for p_ in p_next]
            next_moves[str([i,d,n])] = i_next  
        print("- Mapped. t =", time.time()-t1)
    return map_size, i_dict, next_moves

def total_valid_walks(start,n,m):
    d = len(start)
    n_half = int((n+1)/2)
    for i in range(d):
        if (start[i] > n_half-1):
            start[i] = n-1-start[i]

    map_size, i_dict, next_move = full_map(d,n)
        
    a_this = np.zeros(map_size)
    a_this[i_dict[str([np.array(start),n_half])]] = 1
    while m:
        m -= 1
        a_next = np.zeros(map_size)
        for i,a in enumerate(a_this):
            if(a>0):
                p_next = next_move[str([i,d,n])]
                for p in p_next:
                    a_next[p] += a
        a_this = a_next

    return np.sum(a_this)


def walk_nd_weight_all(d, d_dep, n, n_dep, m, pos, count, tstart):
    if (d_dep):
        for i in range(n_dep,int((n+1)/2)):
            pos[d-d_dep:] = i#*np.ones(d_dep)
            count = walk_nd_weight_all(d, d_dep-1, n, i, m, pos, count, tstart)
    else:
        wk = total_valid_walks(np.array(pos),n,m)
        wg = 1
        c_n = countNhalf(np.array(pos),n)
        c_0 = countNhalf(np.array(pos),0)
        if (n%2==1):
            wg = 2**c_n
        wg = uniq_perm(np.array(pos))/wg
        valid_walks.append(wk)
        valid_wghts.append(wg)
        starting.append(str(np.array(pos)))
        count += 1
        if ( (c_0+c_n)==d ):
            print("\rt="+str(round(time.time()-tstart,2))+":av="+str(round((time.time()-tstart)/count,2))+"|"+str(np.array(pos))+"="+str(int(wk))+"("+str(wg)+")",end='\t     \r',flush=True)
        return count
    return count


#-- d=4, n=10, m=10 --#

print("\n --- d=4, n=10, m=10 --- ")

if (not path.exists('random_walks_data_4_10_10.csv')):
    #-- Generate Data (4,10,10) --#

    print("Generating Data ... ")
    valid_walks = []
    valid_wghts = []
    starting = []

    d=4
    n=10
    m=n
    t1 = time.time()
    count = walk_nd_weight_all(d, d, n, 0, m, np.zeros(d,dtype=int), 0, t1)
    print("\nDone in "+str(time.time()-t1)+"seconds, Starting from "+str(count)+" unique points.")

    valid_walks_4_10_10 = valid_walks
    valid_wghts_4_10_10 = valid_wghts
    starting_4_10_10 = starting     

    #-- Save Data (4,10,10) --#

    print("Saving Data ... ")
    d=4
    n=10
    m=n
    print("To file : ","random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

    data = {}
    data["Starting At"] = starting_4_10_10
    data["Valid Walks"] = valid_walks_4_10_10
    data["Weights"] = valid_wghts_4_10_10

    df = pd.DataFrame(data, columns= ["Starting At", "Valid Walks", "Weights"])

    export_csv = df.to_csv("random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv", index=None, header=True)

#-- Load Data (4,10,10) --#

print("Loading Data ...")

d=4
n=10
m=n
print("From file : ","random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

data = pd.read_csv("random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

starting = data["Starting At"]
valid_walks = data["Valid Walks"] 
valid_wghts = data["Weights"] 

data.T

print("\n Q : Consider the case where d=4, n=10, and m=10.")
# 
print("How many valid walks start from the corner (0, 0, 0, 0)?")


v_corner = float(data.loc[data["Starting At"].isin([str(np.array([0,0,0,0]))]), 'Valid Walks'])

print("\n A : ", v_corner, ' [corner]', "\n---- ")

print("\n Q : Consider the case where d=4, n=10, and m=10.")
# 
print("Consider the count of valid walks associated with each possible starting position. What is the ratio of the highest count of valid walks to smallest count of valid walks?")

v_max = np.max(valid_walks)

v_min = np.min(valid_walks)

print("\n A : ", v_max, '[max] / ', v_min, '[min] = ', v_max/v_min, "\n---- ")


print("\n Q : Consider the case where d=4, n=10, and m=10.")
# 
print("Consider the count of valid walks associated with each possible starting position. What is the ratio of the standard deviation of the number of valid walks to the mean of the number of valid walks?")

mean_walks = np.sum(valid_walks*valid_wghts/np.sum(valid_wghts))

stdev_walks = np.sqrt(np.sum(valid_wghts*((valid_walks-mean_walks)**2))/np.sum(valid_wghts))

print("\n A : ", stdev_walks, '[stdev] / ', mean_walks, '[mean] = ', stdev_walks/mean_walks, "\n---- ")


#-- d=8, n=12, m=12 --#

print("\n --- d=8, n=12, m=12 --- ")

if (not path.exists('random_walks_data_8_12_12.csv')):
    #-- Generate Data (8,12,12) --#

    print("Generating Data ... (this wwon't take much time)")
    valid_walks = []
    valid_wghts = []
    starting = []

    d=8
    n=12
    m=n

    t1 = time.time()
    count = walk_nd_weight_all(d, d, n, 0, m, np.zeros(d,dtype=int), 0, t1)
    print("\nDone in "+str(time.time()-t1)+"seconds, Starting from "+str(count)+" unique points.")

    print(' ')

    valid_walks_8_12_12 = valid_walks
    valid_wghts_8_12_12 = valid_wghts
    starting_8_12_12 = starting     

    #-- Save Data (8,12,12) --#

    print("Saving Data ... ")
    d=8
    n=12
    m=n
    print("To file : ","random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

    data = {}
    data["Starting At"] = starting_8_12_12
    data["Valid Walks"] = valid_walks_8_12_12
    data["Weights"] = valid_wghts_8_12_12

    df = pd.DataFrame(data, columns= ["Starting At", "Valid Walks", "Weights"])

    export_csv = df.to_csv("random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv", index=None, header=True)

#-- Load Data (8,12,12) --#

print("Loading Data ...")

d=8
n=12
m=n
print("From file : ","random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

data = pd.read_csv("random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

starting = data["Starting At"]
valid_walks = data["Valid Walks"] 
valid_wghts = data["Weights"] 

data.T

print("\n Q : Now, let's consider the case where d=8, n=12, and m=12.")
# 
print("How many valid walks start from one of the corners?")


v_corner = float(data.loc[data["Starting At"].isin([str(np.array([0,0,0,0,0,0,0,0]))]), 'Valid Walks'])

print("\n A : ", v_corner, ' [corner]', "\n---- ")

print("\n Q : Consider the case where d=8, n=12, and m=12.")
# 
print("Consider the count of valid walks associated with each possible starting position. What is the ratio of the highest count of valid walks to smallest count of valid walks?")

v_max = np.max(valid_walks)

v_min = np.min(valid_walks)

print("\n A : ", v_max, '[max] / ', v_min, '[min] = ', v_max/v_min, "\n---- ")


print("\n Q : Consider the case where d=8, n=12, and m=12.")
# 
print("Consider the count of valid walks associated with each possible starting position. What is the ratio of the standard deviation of the number of valid walks to the mean of the number of valid walks?")

mean_walks = np.sum(valid_walks*valid_wghts/np.sum(valid_wghts))

stdev_walks = np.sqrt(np.sum(valid_wghts*((valid_walks-mean_walks)**2))/np.sum(valid_wghts))

print("\n A : ", stdev_walks, '[stdev] / ', mean_walks, '[mean] = ', stdev_walks/mean_walks, "\n---- ")


#-- d=8, n=11, m=11 --#

print("\n --- d=8, n=11, m=11 --- ")

if (not path.exists('random_walks_data_8_11_11.csv')):
    #-- Generate Data (8,11,11) --#

    print("Generating Data ... (this won't take much time)")
    valid_walks = []
    valid_wghts = []
    starting = []

    d=8
    n=11
    m=n
    t1 = time.time()
    count = walk_nd_weight_all(d, d, n, 0, m, np.zeros(d,dtype=int), 0, t1)
    print("\nDone in "+str(time.time()-t1)+"seconds, Starting from "+str(count)+" unique points.")

    valid_walks_8_11_11 = valid_walks
    valid_wghts_8_11_11 = valid_wghts
    starting_8_11_11 = starting     

    #-- Save Data (8,11,11) --#

    print("Saving Data ... ")
    d=8
    n=11
    m=n
    print("To file : ","random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

    data = {}
    data["Starting At"] = starting_8_11_11
    data["Valid Walks"] = valid_walks_8_11_11
    data["Weights"] = valid_wghts_8_11_11

    df = pd.DataFrame(data, columns= ["Starting At", "Valid Walks", "Weights"])

    export_csv = df.to_csv("random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv", index=None, header=True)

#-- Load Data (8,11,11) --#

print("Loading Data ...")

d=8
n=11
m=n
print("From file : ","random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

data = pd.read_csv("random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

starting = data["Starting At"]
valid_walks = data["Valid Walks"] 
valid_wghts = data["Weights"] 

data.T

print("\n Q : Now, let's consider the case where d=8, n=11, and m=11.")
# 
print("How many valid walks start from one of the corners?")


v_corner = float(data.loc[data["Starting At"].isin([str(np.array([0,0,0,0,0,0,0,0]))]), 'Valid Walks'])

print("\n A : ", v_corner, ' [corner]', "\n---- ")

print("\n Q : Consider the case where d=8, n=11, and m=11.")
# 
print("Consider the count of valid walks associated with each possible starting position. What is the ratio of the highest count of valid walks to smallest count of valid walks?")

v_max = np.max(valid_walks)

v_min = np.min(valid_walks)

print("\n A : ", v_max, '[max] / ', v_min, '[min] = ', v_max/v_min, "\n---- ")


print("\n Q : Consider the case where d=8, n=11, and m=11.")
# 
print("Consider the count of valid walks associated with each possible starting position. What is the ratio of the standard deviation of the number of valid walks to the mean of the number of valid walks?")

mean_walks = np.sum(valid_walks*valid_wghts/np.sum(valid_wghts))

stdev_walks = np.sqrt(np.sum(valid_wghts*((valid_walks-mean_walks)**2))/np.sum(valid_wghts))

print("\n A : ", stdev_walks, '[stdev] / ', mean_walks, '[mean] = ', stdev_walks/mean_walks, "\n---- ")


#----#

input()
