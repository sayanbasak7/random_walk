#!/usr/bin/env python
# coding: utf-8

# ## Section 2:
# ### Consider a grid in d-dimensional space. There are n grid lines in each dimension, spaced one unit apart. We will consider a walk of m steps from grid intersection to grid intersection. Each step will be a single unit movement in any one of the dimensions, such that it says on the grid. We wish to look at the number of possible paths from a particular starting location on this grid.
# 
# ### For example, consider the case where d=2 and n=3. We will label the grid intersections (x,y), where x,y E {0,1,2}. There will be six valid walks starting at (0,0) of length m=2:
# 
# * (0,0)->(0,1)->(0,0)
# * (0,0)->(0,1)->(0,2)
# * (0,0)->(0,1)->(1,1)
# * (0,0)->(1,0)->(0,0)
# * (0,0)->(1,0)->(2,0)
# * (0,0)->(1,0)->(1,1)
# 
# ### Note that the walks may double back upon themselves, and multiple walks may end at the same grid intersection. All of these walks are counted.


import pandas as pd
import numpy as np
import time
from os import path

#------------------- Some key points -------------------#

# These functions and algorithms are defined keeping in mind the symmetries associated with the given lattice.
# Configurationally the site index 'i' is equivalent to 'n-i', hence for even number of lattice sites for each degree of freedom, the relevant sites are reduced by a factor 2^d.
# Also, another reduction in the number of relevant sites is achieved by:
# The number of valid walks originating from (i1,i2,...,id) s.t. i1 <= i2 <= ... <= id, is same for all permutations of (i1,i2,...,id).

#-------------------------------------------------------#


#-- Functions --#

def position_indexing(pos, d, n):
    return np.sum(pos*(n**np.array(range(d),dtype=int)[::-1]), axis=-1)

def index_positioning(indx, d, n):
    return [int(indx/(n**(d-1-i)))%n for i in range(d)]

def perm(s):
    if len(s)==0:
        return 1
    return len(s)*perm(s[1:])/s.count(s[0])

def uniq_perm(x):
    s = ''.join([chr(xi+ord('A')) for xi in x])
    return int(perm(s))

def next_locs(start, d, n):
    locs = []
    move = np.diag(np.ones(d,dtype=int))
    for i in range(d):
        if (start[i] > 0):
            locs.append(np.sort(start-move[i]))
        if (start[i] < int((n-1)/2)):
            locs.append(np.sort(start+move[i]))
        else:
            if (start[i] < n-1):
                x = start
                x[i] = n-1-x[i]-1
                locs.append(np.sort(x))
    return locs

def full_map(d,n):
    n_half = int((n+1)/2)
    map_size = 0
    i_dict = {}
    p_dict = {}
    next_moves = {}
    for i in range(n_half**d):
        p = index_positioning(i,d,n_half)
        p = np.sort(p)
        try:
            i_dict[str(np.array(p))]
        except:
            i_dict[str(np.array(p))] = map_size
            p_dict[str(map_size)] = p
            map_size += 1

    for i in range(map_size):
        p = p_dict[str(i)]
        p_next = next_locs(p,d,n)
        i_next = [i_dict[str(np.array(p_))] for p_ in p_next]
        next_moves[str(i)] = i_next  
    return map_size, i_dict, next_moves

nom_dict = {}
def total_valid_walks(start,n,m): 
    d = len(start)
    n_half = int((n+1)/2)
    for i in range(d):
        if (start[i] > n_half-1):
            start[i] = n-1-start[i]
            
    try:
        t1 = time.time()
        map_size, i_dict, next_move = nom_dict[str([d,n])]
    except:
        t1 = time.time()
        map_size, i_dict, next_move = full_map(d,n)
        nom_dict[str([d,n])] = map_size, i_dict, next_move
        
    a_this = np.zeros(map_size)
    a_this[i_dict[str(np.array(start))]] = 1
    while m:
        m -= 1
        a_next = np.zeros(map_size)
        for i,a in enumerate(a_this):
            if(a>0):
                p_next = next_move[str(i)]
                for p in p_next:
                    a_next[p] += a

        a_this = a_next
#     print(a_this)
    return np.sum(a_this)

#-- d=4, n=10, m=10 --#

print("\n --- d=4, n=10, m=10 --- ")

if (not path.exists('random_walks_data_4_10_10.csv')):
    #-- Generate Data (4,10,10) --#

    print("Generating Data ... ")
    valid_walks = []
    valid_wghts = []
    starting = []

    d=4
    n=10
    m=n

    tstart=time.time()
    for i1 in range(0,int(n/2)):
        for i2 in range(i1,int(n/2)):
            for i3 in range(i2,int(n/2)):
                for i4 in range(i3,int(n/2)):
                    wk = total_valid_walks([i1,i2,i3,i4],n,m)
                    wg = uniq_perm([i1,i2,i3,i4])
                    valid_walks.append(wk)
                    valid_wghts.append(wg)
                    starting.append(str([i1,i2,i3,i4]))
                    print("\rTime elapsed = "+str(time.time()-tstart)+' s : '+str([i1,i2,i3,i4]),end='               ')

    print(' ')

    valid_walks_4_10_10 = valid_walks
    valid_wghts_4_10_10 = valid_wghts
    starting_4_10_10 = starting     

    #-- Save Data (4,10,10) --#

    print("Saving Data ... (to save time in the next run)")
    d=4
    n=10
    m=n
    print("To file : ","random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

    data = {}
    data["Starting At"] = starting_4_10_10
    data["Valid Walks"] = valid_walks_4_10_10
    data["Weights"] = valid_wghts_4_10_10

    df = pd.DataFrame(data, columns= ["Starting At", "Valid Walks", "Weights"])

    export_csv = df.to_csv("random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv", index=None, header=True)

#-- Load Data (4,10,10) --#

print("Loading Data ...")

d=4
n=10
m=n
print("From file : ","random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

data = pd.read_csv("random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

starting = data["Starting At"]
valid_walks = data["Valid Walks"] 
valid_wghts = data["Weights"] 

data.T

print("\n Q : Consider the case where d=4, n=10, and m=10.")
# 
print("How many valid walks start from the corner (0, 0, 0, 0)?")


v_corner = float(data.loc[data["Starting At"].isin([str([0,0,0,0])]), 'Valid Walks'])

print("\n A : ", v_corner, ' [corner]', "\n---- ")

print("\n Q : Consider the case where d=4, n=10, and m=10.")
# 
print("Consider the count of valid walks associated with each possible starting position. What is the ratio of the highest count of valid walks to smallest count of valid walks?")

v_max = np.max(valid_walks)

v_min = np.min(valid_walks)

print("\n A : ", v_max, '[max] / ', v_min, '[min] = ', v_max/v_min, "\n---- ")


print("\n Q : Consider the case where d=4, n=10, and m=10.")
# 
print("Consider the count of valid walks associated with each possible starting position. What is the ratio of the standard deviation of the number of valid walks to the mean of the number of valid walks?")

mean_walks = np.sum(valid_walks*valid_wghts/np.sum(valid_wghts))

stdev_walks = np.sqrt(np.sum(valid_wghts*((valid_walks-mean_walks)**2))/np.sum(valid_wghts))

print("\n A : ", stdev_walks, '[stdev] / ', mean_walks, '[mean] = ', stdev_walks/mean_walks, "\n---- ")


#-- d=8, n=12, m=12 --#

print("\n --- d=8, n=12, m=12 --- ")

if (not path.exists('random_walks_data_8_12_12.csv')):
    #-- Generate Data (8,12,12) --#

    print("Generating Data ... (this will take some time)")
    valid_walks = []
    valid_wghts = []
    starting = []

    d=8
    n=12
    m=n

    tstart=time.time()
    for i1 in range(0,int(n/2)):
        for i2 in range(i1,int(n/2)):
            for i3 in range(i2,int(n/2)):
                for i4 in range(i3,int(n/2)):
                    for i5 in range(i4,int(n/2)):
                        for i6 in range(i5,int(n/2)):
                            for i7 in range(i6,int(n/2)):
                                for i8 in range(i7,int(n/2)):
                                    wk = total_valid_walks([i1,i2,i3,i4,i5,i6,i7,i8],n,m)
                                    wg = uniq_perm([i1,i2,i3,i4,i5,i6,i7,i8])
                                    valid_walks.append(wk)
                                    valid_wghts.append(wg)
                                    starting.append(str([i1,i2,i3,i4,i5,i6,i7,i8]))
                                    print("\rTime elapsed = "+str(time.time()-tstart)+' s : '+str([i1,i2,i3,i4,i5,i6,i7,i8]),end='               ')

    print(' ')

    valid_walks_8_12_12 = valid_walks
    valid_wghts_8_12_12 = valid_wghts
    starting_8_12_12 = starting     

    #-- Save Data (8,12,12) --#

    print("Saving Data ... (to save time in the next run)")
    d=8
    n=12
    m=n
    print("To file : ","random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

    data = {}
    data["Starting At"] = starting_8_12_12
    data["Valid Walks"] = valid_walks_8_12_12
    data["Weights"] = valid_wghts_8_12_12

    df = pd.DataFrame(data, columns= ["Starting At", "Valid Walks", "Weights"])

    export_csv = df.to_csv("random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv", index=None, header=True)

#-- Load Data (8,12,12) --#

print("Loading Data ...")

d=8
n=12
m=n
print("From file : ","random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

data = pd.read_csv("random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

starting = data["Starting At"]
valid_walks = data["Valid Walks"] 
valid_wghts = data["Weights"] 

data.T

print("\n Q : Now, let's consider the case where d=8, n=12, and m=12.")
# 
print("How many valid walks start from one of the corners?")


v_corner = float(data.loc[data["Starting At"].isin([str([0,0,0,0,0,0,0,0])]), 'Valid Walks'])

print("\n A : ", v_corner, ' [corner]', "\n---- ")

print("\n Q : Consider the case where d=8, n=12, and m=12.")
# 
print("Consider the count of valid walks associated with each possible starting position. What is the ratio of the highest count of valid walks to smallest count of valid walks?")

v_max = np.max(valid_walks)

v_min = np.min(valid_walks)

print("\n A : ", v_max, '[max] / ', v_min, '[min] = ', v_max/v_min, "\n---- ")


print("\n Q : Consider the case where d=8, n=12, and m=12.")
# 
print("Consider the count of valid walks associated with each possible starting position. What is the ratio of the standard deviation of the number of valid walks to the mean of the number of valid walks?")

mean_walks = np.sum(valid_walks*valid_wghts/np.sum(valid_wghts))

stdev_walks = np.sqrt(np.sum(valid_wghts*((valid_walks-mean_walks)**2))/np.sum(valid_wghts))

print("\n A : ", stdev_walks, '[stdev] / ', mean_walks, '[mean] = ', stdev_walks/mean_walks, "\n---- ")


#-- d=8, n=11, m=11 --#

print("\n --- d=8, n=11, m=11 --- ")

if (not path.exists('random_walks_data_8_11_11.csv')):
    #-- Generate Data (8,11,11) --#

    print("Generating Data ... (this may take some time)")
    valid_walks = []
    valid_wghts = []
    starting = []

    d=8
    n=11
    m=n

    tstart=time.time()
    for i1 in range(0,int(n/2)):
        for i2 in range(i1,int(n/2)):
            for i3 in range(i2,int(n/2)):
                for i4 in range(i3,int(n/2)):
                    for i5 in range(i4,int(n/2)):
                        for i6 in range(i5,int(n/2)):
                            for i7 in range(i6,int(n/2)):
                                for i8 in range(i7,int(n/2)):
                                    wk = total_valid_walks([i1,i2,i3,i4,i5,i6,i7,i8],n,m)
                                    wg = 1
                                    if (n%2==1):
                                        wg = 2**countNhalf([i1,i2,i3,i4,i5,i6,i7,i8],n)

                                    wg = uniq_perm([i1,i2,i3,i4,i5,i6,i7,i8])/wg
                                    valid_walks.append(wk)
                                    valid_wghts.append(wg)
                                    starting.append(str([i1,i2,i3,i4,i5,i6,i7,i8]))
                                    print("\rTime elapsed = "+str(time.time()-tstart)+' s : '+str([i1,i2,i3,i4,i5,i6,i7,i8]),end='               ')

    print(' ')

    valid_walks_8_11_11 = valid_walks
    valid_wghts_8_11_11 = valid_wghts
    starting_8_11_11 = starting     

    #-- Save Data (8,11,11) --#

    print("Saving Data ... (to save time in the next run)")
    d=8
    n=11
    m=n
    print("To file : ","random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

    data = {}
    data["Starting At"] = starting_8_11_11
    data["Valid Walks"] = valid_walks_8_11_11
    data["Weights"] = valid_wghts_8_11_11

    df = pd.DataFrame(data, columns= ["Starting At", "Valid Walks", "Weights"])

    export_csv = df.to_csv("random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv", index=None, header=True)

#-- Load Data (8,11,11) --#

print("Loading Data ...")

d=8
n=11
m=n
print("From file : ","random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

data = pd.read_csv("random_walks_data_"+str(d)+"_"+str(n)+"_"+str(m)+".csv")

starting = data["Starting At"]
valid_walks = data["Valid Walks"] 
valid_wghts = data["Weights"] 

data.T

print("\n Q : Now, let's consider the case where d=8, n=11, and m=11.")
# 
print("How many valid walks start from one of the corners?")


v_corner = float(data.loc[data["Starting At"].isin([str([0,0,0,0,0,0,0,0])]), 'Valid Walks'])

print("\n A : ", v_corner, ' [corner]', "\n---- ")

print("\n Q : Consider the case where d=8, n=11, and m=11.")
# 
print("Consider the count of valid walks associated with each possible starting position. What is the ratio of the highest count of valid walks to smallest count of valid walks?")

v_max = np.max(valid_walks)

v_min = np.min(valid_walks)

print("\n A : ", v_max, '[max] / ', v_min, '[min] = ', v_max/v_min, "\n---- ")


print("\n Q : Consider the case where d=8, n=11, and m=11.")
# 
print("Consider the count of valid walks associated with each possible starting position. What is the ratio of the standard deviation of the number of valid walks to the mean of the number of valid walks?")

mean_walks = np.sum(valid_walks*valid_wghts/np.sum(valid_wghts))

stdev_walks = np.sqrt(np.sum(valid_wghts*((valid_walks-mean_walks)**2))/np.sum(valid_wghts))

print("\n A : ", stdev_walks, '[stdev] / ', mean_walks, '[mean] = ', stdev_walks/mean_walks, "\n---- ")


#----#

input()
