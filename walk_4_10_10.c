#include <stdio.h>
#include <time.h>
// #include <omp.h>

// using namespace std;



int main(int argc)
{
    clock_t tstart;
        
    tstart = clock();
       

    double grid[2][12][12][12][12] = {0.0};
    grid[0][1][1][1][1] = 1.0;
    int swap = 0;
    int m = argc;
    int i1,i2,i3,i4,i5,i6,i7,i8;
    printf("grid %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf \n", 
    grid[0][1][1][1][1],
    grid[0][2][2][2][2],
    grid[0][3][3][3][3],
    grid[0][4][4][4][4],
    grid[0][5][5][5][5],
    grid[0][6][6][6][6],
    grid[0][7][7][7][7],
    grid[0][8][8][8][8],
    grid[0][9][9][9][9],
    grid[0][10][10][10][10]);

    for(i1=1; i1<11; i1++)
    {
        for(i2=1; i2<11; i2++)
        {
            for(i3=1; i3<11; i3++)
            {
                for(i4=1; i4<11; i4++)
                {
                    for(i5=1; i5<11; i5++)
                    {
                        for(i6=1; i6<11; i6++)
                        {
                            for(i7=1; i7<11; i7++)
                            {
                                for(i8=1; i8<11; i8++)
                                {
                                    grid[swap][i1][i2][i3][i4] = 0.0;

                                    grid[!swap][i1][i2][i3][i4] = 0.0;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    grid[0][1][1][1][1] = 1.0;
        
    while(m)
    {
        // #pragma omp parallel for private(i1,i2,i3,i4,i5,i6,i7,i8) reduction(+:grid[:2][:12][:12][:12][:12][:12][:12][:12][:12])
        for(i1=1; i1<11; i1++)
        {
            for(i2=1; i2<11; i2++)
            {
                for(i3=1; i3<11; i3++)
                {
                    for(i4=1; i4<11; i4++)
                    {
                        for(i5=1; i5<11; i5++)
                        {
                            for(i6=1; i6<11; i6++)
                            {
                                for(i7=1; i7<11; i7++)
                                {
                                    for(i8=1; i8<11; i8++)
                                    {
                                        long int value = grid[swap][i1][i2][i3][i4];

                                        grid[!swap][i1][i2][i3][i4+1] += value;
                                        grid[!swap][i1][i2][i3+1][i4] += value;
                                        grid[!swap][i1][i2+1][i3][i4] += value;
                                        grid[!swap][i1+1][i2][i3][i4] += value;

                                        grid[!swap][i1][i2][i3][i4-1] += value;
                                        grid[!swap][i1][i2][i3-1][i4] += value;
                                        grid[!swap][i1][i2-1][i3][i4] += value;
                                        grid[!swap][i1-1][i2][i3][i4] += value;
                                        
                                        grid[swap][i1][i2][i3][i4] = 0.0;

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        printf("\rgrid[%d][%d][%d][%d][%d][%d][%d][%d] (%d) \t - time elapsed = %f ", i1, i2, i3, i4, i5, i6, i7, i8, m, ((double) (clock() - tstart)) / CLOCKS_PER_SEC);
        fflush(stdout);
        swap = !swap;
        m = m-1;
    }

    double total_walks = 0.0;
    // #pragma omp parallel for private(i1,i2,i3,i4,i5,i6,i7,i8) reduction(+:total_walks)
    for(i1=1; i1<11; i1++)
    {
        for(i2=1; i2<11; i2++)
        {
            for(i3=1; i3<11; i3++)
            {
                for(i4=1; i4<11; i4++)
                {

                                    total_walks += grid[swap][i1][i2][i3][i4];

                }
            }
        }
    }
    
    printf("\ntotal valid walks : %lf  - time elapsed = %lf\n", total_walks, ((double) (clock() - tstart)) / CLOCKS_PER_SEC);
    return 0;

}